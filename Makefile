gen : main.cpp
	g++ main.cpp -Ofast -march=native -std=c++11 -o genfind

debug : main.cpp
	g++ main.cpp -g -std=c++11 -o genfind
