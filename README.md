# genfind 0.1

PLEASE NOTE: genfind is in a very early stage of development. It may, and probably does, contain bugs. Explore the source code at your own peril!

genfind is a clone of David Eppstein's [gfind](https://www.ics.uci.edu/~eppstein/ca/gfind.c) designed to search a broader class of cellular automata for spaceships than other similar programs support, in particular rules with more than two states. (genfind only supports searches for orthogonal ships at the moment.) See [Eppstein's paper](https://arxiv.org/pdf/cs/0004003.pdf) for more information about the underlying algorithm.

All compile-time options for configuring genfind (including the rule and the width) are located in `rule.cpp`. The file is currently set up to easily search for rules in the Generations rulespace, but you can specify any rule you want by changing the body of the `evolveCells` function. You can then compile genfind by running `make` if you are on a POSIX OS or compatability layer.

Run the program as follows: `./genfind k p s`, where `p` is the spaceship's period, `k` is the number of cells the spaceship "jumps" per period, and `s` is symmetry. (`0` for asymmetric, `1` for odd-width bilateral symmetry, and `2` for even-width bilateral symmetry.) For example, `./genfind 1 3 2` will output the [Turtle](https://www.conwaylife.com/wiki/Turtle) provided that the rule is Life and the width is at least 6.
