/**
 * This file contains all of the logic except for the compile-time parameters (rule, width, and size limits).
 */

#include <iostream>
#include <queue>
#include <stack>
#include <deque>
#include <vector>
#include <cstdlib>
#include <unordered_set>
#include <bitset>
#include <cmath>
#include <cstring>
#include <chrono>
#include <algorithm>

#include "rule.cpp"

#define BITSETSIZE 128

#define SYMMETRY_A 0
#define SYMMETRY_U 1
#define SYMMETRY_V 2

#define STATEMASK  0x80000000
#define LCHILDMASK 0x40000000
#define RCHILDMASK 0x20000000
#define ACHILDMASK 0x60000000
#define DEPTHMASK  0x1fffffff

// node structure for tree
// contains pointer to parent, current depth, presence/absense of left/right children, and which child it is itself
struct node
{
  node* parent;
	uint32_t depth;
  uint32_t children; // shouldn't overflow on any feasible search space
  char row[WIDTH];

    node(int d, char* newRow) // root
    {
        parent = NULL;
        children = 0;
        depth = d;
        for(int i = 0; i < WIDTH; i++)
        {
            row[i] = newRow[i];
        }
    };

    node(node* p, char* newRow)
    {
        parent = p;
        depth = p->depth + 1;
        children = 0;
        for(int i = 0; i < WIDTH; i++)
        {
            row[i] = newRow[i];
        }
    };
};

static int prevTargetDepth;
unsigned int numNodes = 0;

static std::queue<node*> nodequeue; // for bfs search
static std::stack<node*> nodestack; // for dfs search

static int k, p, s; // looking for speed kc/p spaceships
static int* backOff; // use this instead of k in case gcd(k,p) > 1
static int* fwdOff;
static int* doubleOff;

static char** trans; // transposition table
static unsigned long long int transmask; // only look at bottom 4*p bits of state with this
static bool* inuse; // no need to use mask if a table index hasn't been used before

static char** laTrans; // avoid duplicates when using lookahead
static bool* laInUse;
static std::queue<int> laIDsInUse;

// i feel like anyone else looking through the code deserves an apology for this, or at least an explanation
// Eppstein's algorithm uses edge tables to create graphs that are traversed to quickly find successor rows
// both the size and number of tables are dependent on the number of states
// the number of states is set at compile tile mainly so we can use bitsets for the tables; this monstrosity is the result
static std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>* edgetableL0;
static std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>* edgetableL1;
static std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>* edgetableL2;


// hashing functions for transposition table

int djb2(char* str)
{
	uint64_t hash = 5381;
	for(int i = 0; i < WIDTH; i++)
	{
		hash = ((hash<<5)+hash)+str[i];
	}
	return hash % (1<<LATRANSSIZE);
}

int djb2t(char* row, unsigned int inithash=5381)
{
	unsigned int hash = inithash;
	for(int i = 0; i < WIDTH; i++)
	{
		hash = (((hash<<5)+hash)+row[i])% (1<<TRANSPOSITIONSIZE);
	}
	return hash;
}

void setUpEvo()
{
	std::cout<<"Setting up L0 edge table... "<<std::flush;
	edgetableL0 = new std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>[NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES];
	for(int i = 0; i < std::pow(NUM_STATES,7); i++)
	{
		int iw = i;

		int cellCP = iw%NUM_STATES; iw/=NUM_STATES; // c-prime, center evolves into this
		int cellSE = iw%NUM_STATES; iw/=NUM_STATES;
		int cellS  = iw%NUM_STATES; iw/=NUM_STATES;
		int cellSW = iw%NUM_STATES; iw/=NUM_STATES;
		int cellE  = iw%NUM_STATES; iw/=NUM_STATES;
		int cellC  = iw%NUM_STATES; iw/=NUM_STATES;
		int cellW  = iw%NUM_STATES; 

		for(int j = 0; j < NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES; j++)
		{
			int jw = j;

			int cellNE = jw%NUM_STATES; jw/=(NUM_STATES*NUM_STATES);
			int cellN  = jw%NUM_STATES; jw/=(NUM_STATES*NUM_STATES);
			int cellNW = jw%NUM_STATES;

			int cellCPP = evolveCells(cellNW,cellN,cellNE,cellW,cellC,cellE,cellSW,cellS,cellSE);
			edgetableL0[i][j] = (cellCP == cellCPP);
		}

	}
	std::cout<<"Done!\n";

	std::cout<<"Setting up L1 edge table... "<<std::flush;
	edgetableL1 = new std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>[NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES];
	for(int i = 0; i < std::pow(NUM_STATES,6); i++)
	{
		int iw = i;

		int cellSE = iw%NUM_STATES; iw/=NUM_STATES;
		int cellS  = iw%NUM_STATES; iw/=NUM_STATES;
		int cellSW = iw%NUM_STATES; iw/=NUM_STATES;
		int cellE  = iw%NUM_STATES; iw/=NUM_STATES;
		int cellC  = iw%NUM_STATES; iw/=NUM_STATES;
		int cellW  = iw%NUM_STATES; 

		for(int j = 0; j < NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES; j++)
		{
			int jw = j/NUM_STATES;

			int cellNE = jw%NUM_STATES; jw/=NUM_STATES;
			int cellCP = jw%NUM_STATES; jw/=NUM_STATES;
			int cellN  = jw%NUM_STATES; jw/=(NUM_STATES*NUM_STATES);
			int cellNW = jw%NUM_STATES;

			int cellCPP = evolveCells(cellNW,cellN,cellNE,cellW,cellC,cellE,cellSW,cellS,cellSE);
			edgetableL1[i][j] = (cellCP == cellCPP);
		}

	}
	std::cout<<"Done!\n";

	// pretty marginal improvement here honestly
	#if DOUBLELOOK
		std::cout<<"Setting up L2 edge table... "<<std::flush;
		edgetableL2 = new std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>[NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES];
		for(int i = 0; i < std::pow(NUM_STATES,5); i++)
		{
			int iw = i;

			int cellSEE = iw%NUM_STATES; iw/=NUM_STATES;
			int cellSE  = iw%NUM_STATES; iw/=NUM_STATES;
			int cellS   = iw%NUM_STATES; iw/=NUM_STATES;
			int cellSW  = iw%NUM_STATES; iw/=NUM_STATES;
			int cellSWW = iw%NUM_STATES; iw/=NUM_STATES;

			bool validEvolution[NUM_STATES*NUM_STATES*NUM_STATES];
			for(int j = 0; j < NUM_STATES*NUM_STATES*NUM_STATES; j++) validEvolution[j] = false;

			for(int j = 0; j < NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES; j++)
			{
				int jw = j;

				int cellNWW = jw%NUM_STATES; jw/=NUM_STATES;
				int cellNW  = jw%NUM_STATES; jw/=NUM_STATES;
				int cellN   = jw%NUM_STATES; jw/=NUM_STATES;
				int cellNE  = jw%NUM_STATES; jw/=NUM_STATES;
				int cellNEE = jw%NUM_STATES; jw/=NUM_STATES;
				int cellCWW = jw%NUM_STATES; jw/=NUM_STATES;
				int cellCW  = jw%NUM_STATES; jw/=NUM_STATES;
				int cellC   = jw%NUM_STATES; jw/=NUM_STATES;
				int cellCE  = jw%NUM_STATES; jw/=NUM_STATES;
				int cellCEE = jw%NUM_STATES; jw/=NUM_STATES;

				int idx = 0;
				idx += evolveCells(cellNWW,cellNW,cellN,cellCWW,cellCW,cellC,cellSWW,cellSW,cellS);
				idx*=NUM_STATES;
				idx += evolveCells(cellNW,cellN,cellNE,cellCW,cellC,cellCE,cellSW,cellS,cellSE);
				idx*=NUM_STATES;
				idx += evolveCells(cellN,cellNE,cellNEE,cellC,cellCE,cellCEE,cellS,cellSE,cellSEE);

				validEvolution[idx] = true;
			}

			for(int j = 0; j < NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES; j++)
			{
				int cellCPW = (j / (NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES)) % NUM_STATES;
				int cellCP  = (j / (NUM_STATES*NUM_STATES*NUM_STATES                      )) % NUM_STATES;
				int cellCPE = (j / (NUM_STATES                                            )) % NUM_STATES;
				int boolidx = 0;

				boolidx*=NUM_STATES; boolidx+=cellCPW;
				boolidx*=NUM_STATES; boolidx+=cellCP ;
				boolidx*=NUM_STATES; boolidx+=cellCPE;

				edgetableL2[i][j] = validEvolution[boolidx];
			}

		}
		std::cout<<"Done!\n";
	#endif
}

bool strdif(char* str1, char* str2)
{
	for(int i = 0; i < WIDTH; i++) if(str1[i]!=str2[i]) return true;
	return false;
}

void printString(char* str)
{
	for(int i = 0; i < WIDTH; i++)std::cout<<(int)str[i];
}

bool hashdif(char* str1)
{
	int hash = djb2(str1);
	if(laInUse[hash])
	{
		if(!strdif(str1, laTrans[hash])) return false;
		else
		{
			for(int i = 0; i < WIDTH; i++) laTrans[hash][i] = str1[i];
			return true;
		}
	}
	else
	{
		laIDsInUse.push(hash);
		laInUse[hash] = true;
		laTrans[hash] = new char[WIDTH];
		for(int i = 0; i < WIDTH; i++) laTrans[hash][i] = str1[i];
		return true;
	}
}


void printState(std::vector<char*>& state)
{
  std::cout << "\n";
  for(char* c : state)
  {
    for(int i = 0; i < WIDTH; i++) std::cout << (int)c[i];
    std::cout << "\n";
  }
}

bool addNewNode(node* parent, std::vector<char*>& currentState, char* newRow, bool isBFS)
{
	static std::hash<std::bitset<BITSETSIZE>> statehash;
	static bool doubleEnabled = (double)k / (double)p < 1.0/2.0;

	int i = (parent->depth)+1;

	node* newNode = new node(parent, newRow);

	if(isBFS)
	{
		// send node off on its merry way
		parent->children++;
		nodequeue.push(newNode);
		numNodes++;
	}
	else // DFS
	{
		nodestack.push(newNode);
	}
	return true;
}



char printBit(char bit)
{
	if(bit == 0) return '.';
	else return (char)(0x40+bit);
}


/**
 * this function is admittedly a bit of a nightmare. it is also recursive
 *
 * the gist of it is that we've built the graph in addChildren that represents all possible successor rows
 * findrows will recursively traverse the graph; adding a new row to newrows whenever it reaches the end
 */
void findrows(std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>* edges, std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES>* nodes, std::vector<char*>& newrows, int idx, std::vector<char*>& currentState, unsigned int currenthash, int step=0, std::vector<int>* currentrow = NULL)
{
	std::vector<int> newvector;
	if(step == 0)
	{
		currentrow = &newvector;
	}
	currentrow->push_back(idx);

	if(s == SYMMETRY_U && step == 1 &&
	((((*currentrow)[0])/(NUM_STATES*NUM_STATES))%NUM_STATES != idx%NUM_STATES || 
	 (((*currentrow)[0])/(NUM_STATES*NUM_STATES*NUM_STATES))%NUM_STATES != (idx/NUM_STATES)%NUM_STATES))
	{
		currentrow->pop_back();
		return;	
	}

	if(step == WIDTH+1) // we've reached something in the last row
	{
		if(idx == 0)
		{
			char* newrow = new char[WIDTH];
			for(int i = 0; i < WIDTH; i++) newrow[i] = (*currentrow)[i]%NUM_STATES;

			if(!hashdif(newrow))
			{
				delete[] newrow;
			}
			else if(TRANSENABLED)
			{
				unsigned int newhash = djb2t(newrow, currenthash);
				if(inuse[newhash])
				{
					// see if there's any difference between what's in the trans table and what we have
					bool difference = false;
					int idx = 0;
					for(int i = 2*p-2; i >= 0; i--)
					{
						for(int j = 0; j < WIDTH; j++)
						{
							if(trans[newhash][idx] != currentState[i][j])
							{
								difference = true;
								break;
							}
							idx++;
						}
						if(difference) break;
					}
					if(!difference)
					{
						for(int j = 0; j < WIDTH; j++)
						{
							if(trans[newhash][idx] != newrow[j])
							{
								difference = true;
								break;
							}
							idx++;
						}
					}

					// if there if a difference replace the trans element with ours
					if(!difference) delete[] newrow;
					else
					{
						int nidx = 0;
						for(int i = 2*p-2; i >= 0; i--)
						{
							for(int j = 0; j < WIDTH; j++)
							{
								trans[newhash][nidx++] = currentState[i][j];
							}
						}
						for(int j = 0; j < WIDTH; j++)
						{
							trans[newhash][nidx++] = newrow[j];
						}
						newrows.push_back(newrow);
					}
				}
				else
				{
					trans[newhash] = new char[2*p*WIDTH];
					inuse[newhash] = true;
					int nidx = 0;
					for(int i = 2*p-2; i >= 0; i--)
					{
						for(int j = 0; j < WIDTH; j++)
						{
							trans[newhash][nidx++] = currentState[i][j];
						}
					}
					for(int j = 0; j < WIDTH; j++)
					{
						trans[newhash][nidx++] = newrow[j];
					}
					newrows.push_back(newrow);

				}
			}
			else newrows.push_back(newrow);
		}
	}
	else
	{
		for(int w = 0; w < NUM_STATES*NUM_STATES; w++)
		{
			if(edges[step][NUM_STATES*NUM_STATES*idx+w] && nodes[step+1][NUM_STATES*NUM_STATES*(idx%(NUM_STATES*NUM_STATES))+w]) findrows(edges, nodes, newrows, NUM_STATES*NUM_STATES*(idx%(NUM_STATES*NUM_STATES))+w, currentState, currenthash, step+1, currentrow);
		}
	}

	currentrow->pop_back();
	return;	
}

/**
 * addChildren, along with findrows and (as much as i'd like to change this in the future) main, contains most of
 * the "core logic" for the tree search: main iterates through rows, addChildren builds a graph representing all
 * of the possible next rows, and findrows iterates through that graph, adding everything it can to the tree
 *
 * most of the lines in addChildren involve calculating the indices for the different edge tables at each width
 */
void addChildren(node* currentNode, std::vector<char*>& currentState, bool isBFS, bool rootPass=false)
{
	static bool foundZero = false;
	static int stateSize = 2*p;
	int numnewadded = 0;
	int i;
	if(!rootPass) i = ((currentNode->depth)+1)%p;
	std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES> edges[WIDTH+1];
	std::bitset<NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES> nodes[WIDTH+2];

	for(int n = 0; n < WIDTH+1; n++)
	{
		int idxL0 = 0;
		int idxL1 = 0;
		int idxL2 = 0;

		if(!rootPass)
		{
			if(n == 0)
			{
				if(s==SYMMETRY_V)
				{
					idxL0 += currentState[p-1][0];
					idxL1 += currentState[fwdOff[i]-1][0];
				}
				else if(s==SYMMETRY_U)
				{
					idxL0 += currentState[p-1][1];
					idxL1 += currentState[fwdOff[i]-1][1];
				}
			}
			else
			{
				idxL0 += currentState[p-1][n-1];
				idxL1 += currentState[fwdOff[i]-1][n-1];
			}
			idxL0*=NUM_STATES;
			idxL1*=NUM_STATES;

			if(n != WIDTH)
			{
				idxL0 += currentState[p-1][n];
				idxL1 += currentState[fwdOff[i]-1][n];
			}
			idxL0*=NUM_STATES;
			idxL1*=NUM_STATES;

			// if we are at the end, don't add anything -- the last cell needs to be blank
			// there are two oob instances here: instance where center cell is the last cell and where the left cell is the last cell
			if(n < WIDTH-1)
			{
				idxL0 += currentState[p-1][n+1];
				idxL1 += currentState[fwdOff[i]-1][n+1];
			}
			idxL0*=NUM_STATES;
			idxL1*=NUM_STATES;

			if(n == 0)
			{
				if(s==SYMMETRY_V)
				{
					idxL0 += currentState[2*p-1][0];
					idxL1 += currentState[p+fwdOff[i]-1][0];
				}
				else if(s==SYMMETRY_U)
				{
					idxL0 += currentState[2*p-1][1];
					idxL1 += currentState[p+fwdOff[i]-1][1];
				}
			}				
			else
			{
				idxL0 += currentState[2*p-1][n-1];
				idxL1 += currentState[p+fwdOff[i]-1][n-1];
			}
			idxL0*=NUM_STATES;
			idxL1*=NUM_STATES;
			
			if(n != WIDTH)
			{
				idxL0 += currentState[2*p-1][n];
				idxL1 += currentState[p+fwdOff[i]-1][n];
			}
			idxL0*=NUM_STATES;
			idxL1*=NUM_STATES;
			
			// same goes here -- remember that we go to a point where the center cell is past the row boundary, so there are two oob instances here
			if(n < WIDTH-1)
			{
				idxL0 += currentState[2*p-1][n+1];
				idxL1 += currentState[p+fwdOff[i]-1][n+1];
			}
			idxL0*=NUM_STATES;
			//idxL1*=NUM_STATES; // don't need to do this

			if(n != WIDTH) idxL0 += currentState[p-backOff[i]-1][n];

			#if DOUBLELOOK
				if(n == 0)
				{
					if(s==SYMMETRY_V)
					{
						idxL2 += currentState[doubleOff[i]-1][1];
						idxL2*=NUM_STATES;
						idxL2 += currentState[doubleOff[i]-1][0];
						idxL2*=NUM_STATES;
					}
					else if(s==SYMMETRY_U)
					{
						idxL2 += currentState[doubleOff[i]-1][2];
						idxL2*=NUM_STATES;
						idxL2 += currentState[doubleOff[i]-1][1];
						idxL2*=NUM_STATES;
					}
					
					idxL2 += currentState[doubleOff[i]-1][0];
					idxL2*=NUM_STATES;
					idxL2 += currentState[doubleOff[i]-1][1];
					idxL2*=NUM_STATES;
					idxL2 += currentState[doubleOff[i]-1][2];
				}
				else if(n == 1)
				{
					if(s==SYMMETRY_V)
					{
						idxL2 += currentState[doubleOff[i]-1][0];
						idxL2*=NUM_STATES;
					}
					else if(s==SYMMETRY_U)
					{
						idxL2 += currentState[doubleOff[i]-1][1];
						idxL2*=NUM_STATES;
					}

					idxL2 += currentState[doubleOff[i]-1][0];
					idxL2*=NUM_STATES;
					idxL2 += currentState[doubleOff[i]-1][1];
					idxL2*=NUM_STATES;
					idxL2 += currentState[doubleOff[i]-1][2];
					idxL2*=NUM_STATES;
					idxL2 += currentState[doubleOff[i]-1][3];
				}
				else
				{
					idxL2 += currentState[doubleOff[i]-1][n-2];
					idxL2*=NUM_STATES;
					idxL2 += currentState[doubleOff[i]-1][n-1];
					idxL2*=NUM_STATES;
				}

				if(n>1)
				{
					if(n   < WIDTH) idxL2 += currentState[doubleOff[i]-1][n];
					idxL2*=NUM_STATES;
					if(n+1 < WIDTH) idxL2 += currentState[doubleOff[i]-1][n+1];
					idxL2*=NUM_STATES;
					if(n+2 < WIDTH) idxL2 += currentState[doubleOff[i]-1][n+2];
				}
			#endif

		}
		#if DOUBLELOOK
			edges[n] = edgetableL0[idxL0] & edgetableL1[idxL1] & edgetableL2[idxL2];
		#else
			edges[n] = edgetableL0[idxL0] & edgetableL1[idxL1];
		#endif
	}

	nodes[WIDTH+1][0] = true;
	for(int m = WIDTH; m >= 0; m--)
	{
		for(int p = 0; p < NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES; p++) // also want to try vector-based
		{
			if(nodes[m+1][p])
			{
				for(int q = 0; q < NUM_STATES*NUM_STATES; q++)
				{
					if(edges[m][NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES*q+p])
					{
						nodes[m][NUM_STATES*NUM_STATES*q + p/(NUM_STATES*NUM_STATES)] = true;
					}
				}
			}
		}
	}
	
	std::vector<char*> newrows;
	
	unsigned int currentHash = 5381;
	for(int j = 2*p-1; j >= 0; j--)
	{
		currentHash = djb2t(currentState[j], currentHash);
	}
	
	if(s == SYMMETRY_V)
	{
		for(int w = 0; w < NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES; w++)
			if((w/(NUM_STATES*NUM_STATES))%NUM_STATES == w%NUM_STATES && (w/(NUM_STATES*NUM_STATES*NUM_STATES))%NUM_STATES == (w/NUM_STATES)%NUM_STATES && nodes[0][w])
				findrows(edges, nodes, newrows, w, currentState, currentHash);
	}
	if(s == SYMMETRY_U)
	{
		for(int w = 0; w < NUM_STATES*NUM_STATES*NUM_STATES*NUM_STATES; w++)
		//	if(w/NUM_STATES == w%NUM_STATES)
			if(nodes[0][w])
				findrows(edges, nodes, newrows, w, currentState, currentHash);
	}
	if(s == SYMMETRY_A)
	{
		if(nodes[0][0])
			findrows(edges, nodes, newrows, 0, currentState, currentHash);
	}

	for(char* r : newrows)
	{	if(rootPass)
		{
			if(!foundZero)
			{
				for(int c = 0; c < WIDTH; c++)
				{
					if(r[c]) break;
					if(c == WIDTH-1) foundZero = true;
				}
				if(foundZero)
                {
				    delete[] r;
				    continue;
                }
			}
			node* root = new node(stateSize, r);

			nodequeue.push(root);
			numNodes++;
		}
		else
		{
			addNewNode(currentNode, currentState, r, isBFS);
		}
		delete[] r;
	}
	
	while(!laIDsInUse.empty())
	{
		int deleteID = laIDsInUse.front();
		laIDsInUse.pop();
		laInUse[deleteID] = false;
		delete[] laTrans[deleteID];
	}
}

int main(int argc, char** argv)
{
    k = atoi(argv[1]);
    p = atoi(argv[2]);
	s = (argc>3) ? atoi(argv[3]) : SYMMETRY_A;

	int stateSize = 2*p;

	// backOff and doubleOff tables are used instead of k in case gcd(p,k) > 1
	// table creation was more or less copied from qfind
	backOff = new int[p];
	for(int i = 0; i < p; i++) backOff[i] = -1;
	int x = 0; // current index
	int y; // value to put in index
	for(int i = 0; i < p-1; i++)
	{
		y = k;
		while(true)
		{
			if(backOff[(x+y)%p] == -1)
			{
				backOff[x] = y;
				x = (x+y)%p;
				break;
			}
			y++;
		}
	}
	y = 1;
	while(true)
	{
		if((x+y)%p == 0)
		{
			backOff[x] = y;
			break;
		}
		y++;
	}

	fwdOff = new int[p];
	for (int i = 0; i < p; i++) fwdOff[(i+backOff[i])%p] = backOff[i];

	doubleOff = new int[p];
	for (int i = 0; i < p; i++)
	{
		int j = i - fwdOff[i];
		if (j < 0) j += p;
		doubleOff[i] = fwdOff[i] + fwdOff[j];
	}

    trans = new char*[1<<TRANSPOSITIONSIZE];
    inuse = new bool[1<<TRANSPOSITIONSIZE];
    for(int i = 0; i < 1<<TRANSPOSITIONSIZE; i++) inuse[i] = false;

	laTrans = new char*[1<<LATRANSSIZE];
	laInUse = new bool[1<<LATRANSSIZE];
	for(int i = 0; i < 1<<LATRANSSIZE; i++) laInUse[i] = false;

    int iters  = 0;
    int miters = 0;

	setUpEvo();

	std::vector<char*> currentState;
	char allZeros[WIDTH];
    for(int x = 0; x < WIDTH; x++) allZeros[x] = 0;
    while(currentState.size() < stateSize) currentState.push_back(allZeros);

	addChildren(NULL, currentState, true, true);

	auto start = std::chrono::high_resolution_clock::now();

	// main search loop
    while(nodequeue.size() > 0)
    {

		// dfs, if necessary
        if(numNodes > (1 << TREESIZE))
        {
            int targetDepth = (nodequeue.back()->depth) + p;
            if(prevTargetDepth >= (nodequeue.back()->depth)) targetDepth = prevTargetDepth + p;
            prevTargetDepth = targetDepth;

            std::cout << "Pruning frontier, Depth: " << targetDepth - (nodequeue.back()->depth)<< "   " << nodequeue.size() << " -> " << std::flush;

            std::queue<node*> newnodequeue;
            int iter = 0;
            int diter = 0;
            while(!nodequeue.empty())
            {
                node* currentNode = nodequeue.front();
				int i = (currentNode->depth)+1;
                nodequeue.pop();

				std::vector<char*> currentState;
				node* currentNodePath = currentNode;

				int x = 0;
				while(x < stateSize && currentNodePath != NULL)
				{
					currentState.push_back(currentNodePath->row);
					currentNodePath = currentNodePath->parent;
				}
                char allZeros[WIDTH];
                for(int x = 0; x < WIDTH; x++) allZeros[x] = 0;
                while(currentState.size() < stateSize) currentState.push_back(allZeros);

                // try to add left and right children of current node
				addChildren(currentNode, currentState, false);

				std::stack<node*> nodetrash;
                while(!nodestack.empty() && (nodestack.top()->depth) < targetDepth)
                {
                    node* currentNodeDFS = nodestack.top();
					int i = (currentNodeDFS->depth)+1;
                    nodestack.pop();

					std::vector<char*> currentStateDFS;
					node* currentNodeDFSPath = currentNodeDFS;

					int y = 0;
					while(y < stateSize && currentNodeDFSPath != NULL)
					{
						currentStateDFS.push_back(currentNodeDFSPath->row);
						currentNodeDFSPath = currentNodeDFSPath->parent;
					}
                    char allZerosDFS[WIDTH];
                    for(int x = 0; x < WIDTH; x++) allZerosDFS[x] = 0;
                    while(currentStateDFS.size() < stateSize) currentStateDFS.push_back(allZerosDFS);

                    // try to add left and right children of current node
					addChildren(currentNodeDFS, currentStateDFS, false);

                    nodetrash.push(currentNodeDFS);
                }

                iter++;
                if(nodestack.empty())
                {
                    diter++;
                    while(true)
                    {
                        if(currentNode == NULL) break;
                        node* parent = currentNode->parent;

                        if(currentNode->children == 0)
                        {
                            if(currentNode->parent != NULL) currentNode->parent->children--;
                            delete currentNode;
                            numNodes--;
                            currentNode = parent;
                        }
                        else break;
                    }
                }
                else
                {
                    while(!nodestack.empty())
                    {
						nodestack.top()->parent = NULL;
                        delete nodestack.top();
                        nodestack.pop();
                    }

                    newnodequeue.push(currentNode);
                }

				while(!nodetrash.empty())
                {
					nodetrash.top()->parent = NULL;
                    delete nodetrash.top();
                    nodetrash.pop();
                }
            }
            nodequeue = newnodequeue;
            std::cout << nodequeue.size() << "\n";
            continue;
        }

        node* currentNode = nodequeue.front();
        nodequeue.pop();

		std::vector<char*> currentState;
		node* currentNodePath = currentNode;
		bool bitsInState = false;
		int x = 0;
		while(currentState.size() < stateSize && currentNodePath != NULL)
		{
			currentState.push_back(currentNodePath->row);
			if(!bitsInState)
			{
				for(int b = 0; b < WIDTH; b++)
				{
					if(currentNodePath->row[b] != 0)
					{
						bitsInState = true;
						break;
					}
				}
			}

		currentNodePath = currentNodePath->parent; /* valgrind: invalid read, nonroot */
		}
    char allZeros[WIDTH];
    for(int x = 0; x < WIDTH; x++) allZeros[x] = 0;
    while(currentState.size() < stateSize) currentState.push_back(allZeros);

        if(!bitsInState && currentNode->depth >= 4*p //|| iters != 0 && nodequeue.empty()
		//	|| currentNode->depth > 200
		)
        {
            std::cout << "Ship found at depth " << (currentNode->depth) << ":\n" << "\nx = 0, y = 0, rule =" << RULENAME << "\n";
            std::cout << "$";
			node* currentNodePath = currentNode;
            while(currentNodePath != NULL)
            {
				if(s != SYMMETRY_A) for(int x = WIDTH-1; x >= 0; x--) std::cout << printBit(currentNodePath->row[x]);
				if(s == SYMMETRY_V) std::cout << printBit(currentNodePath->row[x]);
                for(int x = 1; x < WIDTH; x++) std::cout << printBit(currentNodePath->row[x]);
                std::cout << "$\n";

                for(int x = 0; x < p; x++)
                {
                    if(currentNodePath != NULL)
                    {
                        currentNodePath = currentNodePath->parent;
                    }
                }
            }
            std::cout << "!\n";

			while(true)
			{
				if(currentNode==NULL) break;
				node* parent = currentNode->parent;

				if(currentNode->children == 0)
				{
					if(parent != NULL) parent->children--;
					delete currentNode;
					numNodes--;
					currentNode = parent;
				}
				else break;
			}

			auto stop = std::chrono::high_resolution_clock::now(); 
			auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
			std::cout << "Search took "<<duration.count()/1000000<<" seconds\n";

			delete[] backOff;
			delete[] fwdOff;
			delete[] doubleOff;
			return 0;
        }

        iters++;

		#if VERBOSITY == 1
			if(iters%1000000 == 0)
			{
				iters = 0;
				std::cout << (++miters) << "M\t" << (currentNode->depth) << "\t" << nodequeue.size() << "\n";
			}
		#elif VERBOSITY == 2
			if(iters%1000 == 0)
			{
				iters = 0;
				std::cout << (++miters) << "K\t" << (currentNode->depth) << "\t" << nodequeue.size() << "\n";
			}
		#endif

		int i = (currentNode->depth)+1;

		int oldsize = nodequeue.size();
		addChildren(currentNode, currentState, true);


        // check for dead-end branches
        while(true)
        {
            if(currentNode==NULL) break;
            node* parent = currentNode->parent;

            if(currentNode->children == 0)
            {
                if(parent != NULL) parent->children--;
                delete currentNode;
                numNodes--;
                currentNode = parent;
            }
            else break;
        }
    }

	auto stop = std::chrono::high_resolution_clock::now(); 
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
	std::cout << "Search took "<<duration.count()/1000000<<" seconds\n";
    std::cout << "No spaceships found\n";

    return 0;
}
