/**
 * The evolveCells function will output the evolution of cellC given it and all of
 * its neighbors.
 *
 * This file is set up for easy searching of outer-totalistic generations rules; change
 * BCONDS (birth conditions) SCONDS (survival conditions), NUM_STATES (number of
 * states), and RULENAME as appropriate. The default configuration is 23/3/2, or Life
 *
 * Other compile-time parameters are included here for convinience.
 *
 */

#include <iostream>

// enables double lookahead
// i generally leave this at false since the improvements are marginal
#define DOUBLELOOK (true)

// determines if transposition table is enabled
// setting this to false is equivalent to h0 in gfind/qfind
#define TRANSENABLED (true)

// log of size of transposition table
// equivalent to hNN in gfind/qfind
#define TRANSPOSITIONSIZE 24

// log of size of transposition table used in lookahead
#define LATRANSSIZE 20

// how often genfind outputs the [nodecount  depth  queuesize] triplet
// 0 is never, 1 is every million nodes, 2 is every thousand nodes
#define VERBOSITY 2

// log of max size of tree before dfs
// roughly equivalent to qNN in gfind/qfind
#define TREESIZE 25

// width of search
// equivalent to wNN in qfind and correlates with lNN in gfind/qfind
// IMPORTANT: the "true" width of asymmetric searches is one cell fewer than specified here
#define WIDTH 7

#define NUM_STATES 2

#define BCONDS (n == 3)
#define SCONDS (n == 2 || n == 3)

#define RULENAME "23/3/2"
int evolveCells(int cellNW,int cellN,int cellNE,int cellW,int cellC,int cellE,int cellSW,int cellS,int cellSE)
{
	int n = 0;
	if(cellNW==1)n++;
	if(cellN ==1)n++;
	if(cellNE==1)n++;
	if(cellW ==1)n++;
	if(cellE ==1)n++;
	if(cellSW==1)n++;
	if(cellS ==1)n++;
	if(cellSE==1)n++;

	#if NUM_STATES>2
		switch(cellC)
		{
			case 0:
				if(BCONDS) return 1;
				else return 0;
			case 1:
				if(SCONDS) return 1;
				else return 2;
			case NUM_STATES-1:
				return 0;
			default:
				return cellC+1;
		}
	#else
		switch(cellC)
		{
			case 0:
				if(BCONDS) return 1;
				else return 0;
			case 1:
				if(SCONDS) return 1;
				else return 0;
		}
	#endif
	std::cout << "Error: invalid evolution!\n";
	while(1);
}
